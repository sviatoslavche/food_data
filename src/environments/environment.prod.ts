export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCTvNpSgLCvdrEb0080ngmWi6SuWRnpJv4",
    authDomain: "food-data-fcda6.firebaseapp.com",
    databaseURL: "https://food-data-fcda6.firebaseio.com",
    projectId: "food-data-fcda6",
    storageBucket: "food-data-fcda6.appspot.com",
    messagingSenderId: "32437392370"
  }
};
