// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCTvNpSgLCvdrEb0080ngmWi6SuWRnpJv4",
    authDomain: "food-data-fcda6.firebaseapp.com",
    databaseURL: "https://food-data-fcda6.firebaseio.com",
    projectId: "food-data-fcda6",
    storageBucket: "food-data-fcda6.appspot.com",
    messagingSenderId: "32437392370"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
