import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {IndexComponent} from './index/index.component';
import {ManageComponent} from './manage/manage.component';
import {CreateComponent} from './create/create.component';
import {LoginComponent} from './login/login.component';

import {AuthGuard} from './guards/auth.guard'

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: IndexComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'food/:id',
    component: ManageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: CreateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
