import {Component, OnInit} from '@angular/core';
import {FoodService} from '../services/food.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  title = 'Create food entry!!1';

  angForm: FormGroup;
  constructor(private foodService: FoodService, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      postText: ['', Validators.required ],
      postAuthor: ['', Validators.required ]
    });
  }

  addFood(postText, postAuthor) {
    const dataObj = {
      postText: postText,
      postAuthor: postAuthor
    };
    this.foodService.addFood(dataObj);
  }

  ngOnInit() {
  }

}
