import {Component, OnInit} from '@angular/core';
import {FoodService} from '../services/food.service';
import {FileService} from "../services/file.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Observable} from "rxjs";

import {get} from 'lodash';



@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit {

  public foodItem: Observable<any>;
  public foodId;
  public foodImages = [];

  foodForm: FormGroup = this.fb.group({
    postText:  ['', Validators.required ],
    postAuthor:    ['', Validators.required ]
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private foodService: FoodService,
    private fileService: FileService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.foodItem = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.foodId = params.get('id');
        this.foodItem = this.foodService.getFoodItem(params.get('id'));
        return this.foodItem;
      })
    );
    this.buildForm();
  }

  private buildForm() {
    this.foodItem.subscribe(async data => {
      const food = {
        postText: data.postText || '',
        postAuthor: data.postAuthor || '',
      };
      const postImagesNames = Object.keys(data.postImages);

      this.foodImages = postImagesNames
        // .filter(key => key !== 'authorPhoto')
        .map(key => get(data, `postImages.${key}.downloadUrl`, ''));

      this.foodForm.patchValue(food)
    });
  }

  saveFoodChanges() {
    if (this.foodForm.status != 'VALID') {
      return;
    }

    const data = this.foodForm.value;
    this.foodService.updateFood(this.foodId, data)
  }

  goBack(){
    this.router.navigate(['/home']);
  }

}
