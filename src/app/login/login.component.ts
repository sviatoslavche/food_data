import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  async signInWithGoogle() {
    await this.authService.loginWithGoogle();
    return this.afterSignIn();
  }

  async signInWithFacebook() {
    await this.authService.loginWithFacebook();
    return this.afterSignIn();
  }

  private afterSignIn() {
    this.router.navigate(['/']);
  }

}
