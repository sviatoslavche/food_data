import {Injectable} from '@angular/core';
import {auth, User} from 'firebase/app';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from "rxjs";
import {map, first} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState: Observable<User> = null;

  constructor(private afAuth: AngularFireAuth) {

    this.authState = afAuth.authState;
  }

  isLoggedIn() {
      return this.authState.pipe(first());
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  loginLocal() {
    return this.afAuth.auth.signInWithPopup(new auth.EmailAuthProvider());
  }

  loginWithGoogle(){
    return this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
  }

  loginWithFacebook(){
    return this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider())
  }
}
