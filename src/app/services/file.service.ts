import { Injectable } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private afStorage: AngularFireStorage) { }

  async getPostImage(postUid, name) {
    try {
      return this.afStorage.storage.ref(`${postUid}/${name}`).getDownloadURL();
    } catch (e) {
      return '';
    }
  }

  async getPostImages(postUid, paths) {
    return Promise.all(paths.map(path => this.getPostImage(postUid, path)));
  }

}
