import { Injectable } from '@angular/core';
import { HttpClient } from  "@angular/common/http";

import {Client} from 'elasticsearch-browser';
import {Observable} from "rxjs";

@Injectable()
export class ElasticsearchService {

  private client;
  private index = 'food';
  private type = 'posts';

  constructor(private  httpClient:HttpClient) {
    if (!this.client) {
      this.connect();
    }
  }

  private connect() {
    this.client = new Client({
      host: 'https://user:asZTAG47BsJZ@35.204.92.98',
      log: 'trace'
    });
  }

  isAvailable(): any {
    return this.client.ping({
      requestTimeout: Infinity,
      body: 'hello'
    });
  }

  fullTextSearch(queryText = ''): Observable<any> {
    const customersObservable = this.httpClient
      .post('https://us-central1-food-data-fcda6.cloudfunctions.net/search/foodPost', {queryText});

    return customersObservable;
  }
}
