import {Injectable} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from "rxjs/operators";
import {FileService} from "./file.service";

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  private basePath = '/foodItems';
  public items: Observable<any[]>;
  public item: Observable<any>;

  constructor(private afs: AngularFirestore, private fileService: FileService) { }

  addFood(data) {
    this.afs.collection(this.basePath).add(data);
  }

  getFoodItems(): Observable<any[]> {
    this.items = this.afs.collection(this.basePath).snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map( c => {
            const data = c.payload.doc.data();
            const id = c.payload.doc.id;

            return { id, ...data };
          });
        })
      );

    return this.items;
  }


  updateFood(id:string, data: any) {
    this.afs.collection(this.basePath).doc(id).update(data);
  }

  getFoodItem(id: string): Observable<any> {
    this.item = this.afs.collection(this.basePath).doc(id).snapshotChanges()
      .pipe(
        map(changes => ({ id: changes.payload.id, ...changes.payload.data() }) )
      );

    return this.item;
  }
}
