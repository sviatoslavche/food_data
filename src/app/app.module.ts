import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from  '@angular/common/http';

import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

import {environment} from '../environments/environment';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {IndexComponent} from './index/index.component';
import {ManageComponent} from './manage/manage.component';
import {CreateComponent} from './create/create.component';
import {HeaderComponent} from './header/header.component';
import {FoodService} from './services/food.service';
import {AuthService} from './services/auth.service';
import {FileService} from './services/file.service';
import {ElasticsearchService} from './services/elasticsearch.service';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    ManageComponent,
    CreateComponent,
    HeaderComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    AngularFireAuthModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [FoodService, AuthService, FileService, ElasticsearchService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
