import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable, fromEvent} from "rxjs";
import {FoodService} from '../services/food.service';
import {ElasticsearchService} from '../services/elasticsearch.service'
import {map, distinctUntilChanged, debounceTime} from "rxjs/operators";

import {get} from 'lodash';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  title = 'FoodData';

  public items: Observable<any[]>;

  public elasticItems: any;

  @ViewChild('searchText') searchTextRef: ElementRef;

  searchText: string;   // text to search (email)
  searchResult: any;

  constructor(private foodService: FoodService, private elasticSearchService: ElasticsearchService) { }

  ngOnInit() {
    this.items = this.getFoodItems();
    this.search();
    fromEvent(this.searchTextRef.nativeElement, 'keyup').pipe(
      map((evt: any) => evt.target.value),
      distinctUntilChanged(),
      debounceTime(1000)

    ).subscribe(async (text: string) => {
      this.searchText = text;

      this.search(text)
    })
  }

  getFoodItems() {
    return this.foodService.getFoodItems()
      .pipe(
        map(items => {
          return items.map(item => {
            const {postImages, ...data} = item;
            const authorPhoto = get(item, 'postImages.authorPhoto.downloadUrl', '');
            return {authorPhoto, ...data}
          });
        })
      );
  }

  search(textSearch = '') {
    this.elasticSearchService.fullTextSearch(textSearch)
      .subscribe(data => {
        this.elasticItems = data;
      });
  }

}
